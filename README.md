# firstone
### commands
- My public IP
```bash 
curl ifconfig.io/all
```
#### Use _dmidecode_ para obter informações sobre seu dispositivo (Serial Number e afins)
```bash
sudo dmidecode -s system-product-name

sudo dmidecode -s system-manucfacture

sudo dmidecode -s system-serial-number
```
string keywords are:
  bios-vendor
  bios-version
  bios-release-date
  system-manufacturer
  system-product-name
  system-version
  system-serial-number
  system-uuid
  baseboard-manufacturer
  baseboard-product-name
  baseboard-version
  baseboard-serial-number
  baseboard-asset-tag
  chassis-manufacturer
  chassis-type
  chassis-version
  chassis-serial-number
  chassis-asset-tag
  processor-family
  processor-manufacturer
  processor-version
  processor-frequency

